﻿using System;
using System.Threading;
using Component.Ioc;
using Component.MessageBroker;
using Service.Receiver;

namespace ConsoleB
{
    class Program
    {
        static void Main(string[] args)
        {
            //.net core Ioc implementation
            IocComponent.container = new CoreImplementation();
            IocComponent.container.Register<IReceiverService, ReceiverService>();
            IocComponent.container.Register<IRabbitMqConfiguration>(new RabbitMqConfiguration
            {
                //normally i would load these values from a configuration file or db, but for demo purposes i hardcode them here
                HostName = "localhost",
                Queue = "DemoQueue",
                RoutingKey = "DemoKey",
                ReconnectTimeoutMilliseconds = 1000
            });
            IocComponent.container.Register<IMessageBrokerComponent, RabbitMqImplementation>();
            IocComponent.container.Build();
            
            Start();
        }

        static void Start() {

            //resolve and start service
            var service = IocComponent.container.ResolveFirst<IReceiverService>();

            service.Start((receivedName) => {

                if (string.IsNullOrEmpty(receivedName))
                {
                    Console.WriteLine($"You can't have a blank name?!");
                    return true;
                }

                Console.WriteLine($"Hello {receivedName}, I am your father!");
                return true;
            }, (Exception e) => {

                //implement prefered error loggin here. for demo purposes only writing to console
                Console.WriteLine(e.ToString());

                //sleep one second before trying again.
                Thread.Sleep(1000);
                Start();
            });
            
            Console.Clear();
            Console.WriteLine("Ready to receive messages.");
        }
    }
}
