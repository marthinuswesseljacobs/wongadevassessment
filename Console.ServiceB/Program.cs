﻿using Component.Ioc;
using Component.MessageBroker;
using Service.Receiver;

namespace Console.ServiceB
{
    class Program
    {
        static void Main(string[] args)
        {
            //.net core Ioc implementation
            IocComponent.container = new CoreImplementation();
            IocComponent.container.Register<IReceiverService, ReceiverService>();
            IocComponent.container.Register<IRabbitMqConfiguration>(new RabbitMqConfiguration
            {
                //normally i would load these values from a configuration file or db, but for demo purposes i hardcode them here
                HostName = "localhost",
                Queue = "DemoQueue",
                RoutingKey = "DemoKey"
            });
            IocComponent.container.Register<IMessageBrokerComponent, RabbitMqImplementation>();
            IocComponent.container.Build();

            //resolve and start service
            var service = IocComponent.container.ResolveFirst<IReceiverService>();
            service.Start();
        }
    }
}
