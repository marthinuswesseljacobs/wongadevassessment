﻿namespace Component.MessageBroker
{
    public interface IRabbitMqConfiguration : IMessageBrokerConfiguration
    {
        string Exchange { get; set; }
        string Queue { get; set; }
        string RoutingKey { get; set; }
        string ExchangeType { get; set; }
        int ReconnectTimeoutMilliseconds { get; set; }
    }
}
