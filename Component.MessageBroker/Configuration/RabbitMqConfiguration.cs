﻿namespace Component.MessageBroker
{
    public class RabbitMqConfiguration : IRabbitMqConfiguration
    {
        public string HostName { get; set; }
        public string Exchange { get; set; }
        public string Queue { get; set; }
        public string RoutingKey { get; set; }
        public string ExchangeType { get; set; }
        public int ReconnectTimeoutMilliseconds { get; set; }
    }
}
