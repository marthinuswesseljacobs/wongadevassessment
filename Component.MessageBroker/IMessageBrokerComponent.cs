﻿using System;

namespace Component.MessageBroker
{
    public interface IMessageBrokerComponent
    {
        void Send(string message);
        void Receive(Func<string, bool> callback, Action<Exception> errorHandler);
        void Disconnect();
    }
}
