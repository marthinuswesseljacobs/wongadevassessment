﻿namespace Component.MessageBroker
{
    public class RabbitMqExhangeType
    {
        public const string Direct = "direct";
        public const string Fanout = "fanout";
        public const string Headers = "headers";
        public const string Topic = "topic";
    }
}
