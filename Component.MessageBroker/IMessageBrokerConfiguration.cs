﻿namespace Component.MessageBroker
{
    public interface IMessageBrokerConfiguration
    {
        string HostName { get; set; }
    }
}
