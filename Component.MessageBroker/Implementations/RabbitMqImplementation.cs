﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace Component.MessageBroker
{
    //this is a wrapper for the RabbitMq message broker library
    public class RabbitMqImplementation : IMessageBrokerComponent
    {
        IRabbitMqConfiguration _rabbitMqConfiguration;
        IConnection connection;
        IModel channel;
        ConnectionFactory factory;

        public RabbitMqImplementation(IRabbitMqConfiguration rabbitMqConfiguration)
        {
            _rabbitMqConfiguration = rabbitMqConfiguration;
            factory = new ConnectionFactory() { HostName = _rabbitMqConfiguration.HostName };
        }

        public void Send(string message)
        {
            try
            {
                EnsureConnection();

                if (channel is null || channel.IsClosed)
                    channel = connection.CreateModel();

                channel.ExchangeDeclare(exchange: _rabbitMqConfiguration.Exchange, type: _rabbitMqConfiguration.ExchangeType.ToString(), durable: false, autoDelete: false, arguments: null);
                channel.QueueDeclare(queue: _rabbitMqConfiguration.Queue, durable: false, exclusive: false, autoDelete: false, arguments: null);
                channel.QueueBind(queue: _rabbitMqConfiguration.Queue, exchange: _rabbitMqConfiguration.Exchange, routingKey: _rabbitMqConfiguration.RoutingKey, arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: _rabbitMqConfiguration.Exchange, routingKey: _rabbitMqConfiguration.RoutingKey, basicProperties: null, body: body);

            }
            catch (Exception e)
            {
                //implement prefered error loggin here. for demo purposes only writing to console
                Console.WriteLine(e.ToString());

                //reconnect in designated sleep interval
                Thread.Sleep(_rabbitMqConfiguration.ReconnectTimeoutMilliseconds);
                Send(message);
            }
        }

        public void Receive(Func<string, bool> callback, Action<Exception> errorHandler)
        {
            try
            {
                EnsureConnection();

                if (channel is null || channel.IsClosed)
                    channel = connection.CreateModel();

                channel.QueueDeclare(queue: _rabbitMqConfiguration.Queue, durable: false, exclusive: false, autoDelete: false, arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);

                    var handled = callback.Invoke(message);

                    if (handled)
                        channel.BasicAck(ea.DeliveryTag, false);
                };

                channel.BasicConsume(queue: _rabbitMqConfiguration.Queue, autoAck: false, consumer: consumer);
            }
            catch (Exception e)
            {
                errorHandler.Invoke(e);
            }
        }

        private void EnsureConnection()
        {
            try
            {
                if (connection is null || !connection.IsOpen)
                {
                    connection = factory.CreateConnection();
                    Console.WriteLine("Connection established.");
                }

                connection.ConnectionShutdown += Connection_ConnectionShutdown;

            }
            catch (Exception e)
            {
                //implement prefered error loggin here. for demo purposes only writing to console
                Console.WriteLine(e.ToString());

                //reconnect in designated sleep interval
                Thread.Sleep(_rabbitMqConfiguration.ReconnectTimeoutMilliseconds);
                EnsureConnection();
            }

        }

        private void Connection_ConnectionShutdown(object sender, ShutdownEventArgs ea)
        {
            EnsureConnection();
        }

        public void Disconnect()
        {
            channel.Close();
            connection.Close();
        }
    }
}
