﻿using System;

namespace Service.Sender
{
    public interface ISenderService
    {
        void Start(Func<string> input, Action<string> output);
        void Stop();
    }
}
