﻿using Component.MessageBroker;
using System;

namespace Service.Sender
{
    public class SenderService : ISenderService
    {
        IMessageBrokerComponent _messageBroker;

        public SenderService(IMessageBrokerComponent messageBroker)
        {
            _messageBroker = messageBroker;
        }

        public void Start(Func<string> input, Action<string> output)
        {
            while (true)
            {
                var name = input.Invoke();

                if (!string.IsNullOrEmpty(name))
                {
                    _messageBroker.Send(name);
                    output.Invoke(name);
                }
            }
        }

        public void Stop()
        {
            _messageBroker.Disconnect();
        }
    }
}
