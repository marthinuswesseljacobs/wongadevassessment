using Component.Ioc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace TestIocComponent
{
    #region TestClasses

    public interface ITestA
    {
        string testProperty { get; set; }
    }

    public class TestA : ITestA
    {
        public string testProperty { get; set; }
    }
    
    #endregion

    [TestClass]
    public class TestCoreImplementation
    {
        [TestMethod]
        public void register_type()
        {
            var implementation = new CoreImplementation();
            implementation.Register<ITestA, TestA>();
            implementation.Build();
            var resolved = implementation.ResolveFirst<ITestA>();

            Assert.IsInstanceOfType(resolved, typeof(TestA));
        }

        [TestMethod]
        public void register_instance()
        {
            var implementation = new CoreImplementation();
            implementation.Register<ITestA>(new TestA
            {
                testProperty = "test value"
            });
            implementation.Build();
            var resolved = implementation.ResolveFirst<ITestA>();

            Assert.IsInstanceOfType(resolved, typeof(TestA));
            Assert.AreEqual("test value", resolved.testProperty);
        }

        [TestMethod]
        public void resolve_first()
        {
            var implementation = new CoreImplementation();
            implementation.Register<ITestA>(new TestA
            {
                testProperty = "test value 1"
            });
            implementation.Register<ITestA>(new TestA
            {
                testProperty = "test value 2"
            });
            implementation.Build();
            var resolved = implementation.ResolveFirst<ITestA>();
            
            Assert.IsInstanceOfType(resolved, typeof(TestA));
            Assert.AreEqual("test value 2", resolved.testProperty);
        }

        [TestMethod]
        public void resolve_all()
        {
            var implementation = new CoreImplementation();
            implementation.Register<ITestA>(new TestA
            {
                testProperty = "test value 1"
            });
            implementation.Register<ITestA>(new TestA
            {
                testProperty = "test value 2"
            });
            implementation.Build();
            var resolved = implementation.ResolveAll<ITestA>().ToList();

            Assert.IsInstanceOfType(resolved[0], typeof(TestA));
            Assert.IsInstanceOfType(resolved[1], typeof(TestA));

            Assert.AreEqual("test value 1", resolved[0].testProperty);
            Assert.AreEqual("test value 2", resolved[1].testProperty);
        }
    }
}
