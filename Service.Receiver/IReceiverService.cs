﻿using System;

namespace Service.Receiver
{
    public interface IReceiverService
    {
        void Start(Func<string, bool> receive, Action<Exception> errorHandler);
        void Stop();
    }
}
