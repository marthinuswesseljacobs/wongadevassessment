﻿using Component.MessageBroker;
using System;
using System.Threading;

namespace Service.Receiver
{
    public class ReceiverService : IReceiverService
    {
        IMessageBrokerComponent _messageBroker;

        public ReceiverService(IMessageBrokerComponent messageBroker)
        {
            _messageBroker = messageBroker;
        }

        public void Start(Func<string, bool> receive, Action<Exception> errorHandler)
        {
            _messageBroker.Receive((string receivedName) => {

                var handled = receive.Invoke(receivedName);

                return handled;
            }, errorHandler);
        }


        public void Stop()
        {
            _messageBroker.Disconnect();
        }
    }
}
