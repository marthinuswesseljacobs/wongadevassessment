﻿using System;
using System.Threading;
using Component.Ioc;
using Component.MessageBroker;
using Service.Sender;

namespace ConsoleA
{
    class Program
    {
        static void Main(string[] args)
        {
            //.net core Ioc implementation
            IocComponent.container = new CoreImplementation();
            IocComponent.container.Register<ISenderService, SenderService>();
            IocComponent.container.Register<IRabbitMqConfiguration>(new RabbitMqConfiguration
            {
                //normally i would load these values from a configuration file or db, but for demo purposes i hardcode them here
                HostName = "localhost",
                Exchange = "DemoExchange",
                Queue = "DemoQueue",
                RoutingKey = "DemoKey",
                ExchangeType = RabbitMqExhangeType.Topic,
                ReconnectTimeoutMilliseconds = 1000
            });
            IocComponent.container.Register<IMessageBrokerComponent, RabbitMqImplementation>();
            IocComponent.container.Build();

            Start();
        }
        
        static void Start()
        {
            //resolve and start service
            var service = IocComponent.container.ResolveFirst<ISenderService>();

            Console.Clear();
            string instructions = "Type your name and press enter to send it.";
            Console.WriteLine(instructions);

            service.Start(() =>
            {
                var name = Console.ReadLine();
                return name;
            }, (string name) =>
            {
                Console.WriteLine($"Sending your name as '{name}'.");
                Console.WriteLine(instructions);
            });
        }

    }
}

