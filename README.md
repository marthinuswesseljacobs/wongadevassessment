* Go to the bitbucket Downloads page for this project and click on Download repository.

	* https://bitbucket.org/marthinuswesseljacobs/wongadevassessment/downloads/

* Extract the zip file to your computer.

*  Open the solution file (WongaDevAssessment.sln)

*  Build the solution.

*  Make sure you've got a default RabbitMQ configuration installed and running on localhost with no authentication setup.

*  Start the following 2 console applications.

	* ConsoleA
	* ConsoleB

* Enter you name in ConsoleA, and watch it appear in ConsoleB.

PS : In case your rabbitmq is not located on your local machine, you can change the hostname on line 19 in the Program.cs class for both ConsoleA and ConsoleB.


