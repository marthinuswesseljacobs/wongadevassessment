﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Component.Ioc
{
    //this is a wrapper for the .net core dependency injection library
    public class CoreImplementation : IIocComponent
    {
        private ServiceCollection serviceCollection;
        private ServiceProvider serviceProvider;

        public CoreImplementation()
        {
            serviceCollection = new ServiceCollection();
        }

        public void Register<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService
        {
            serviceCollection.AddSingleton<TService, TImplementation>();
        }

        public void Register<TService>(TService implementation)
            where TService : class
        {
            serviceCollection.AddSingleton(implementation);
        }

        public void Build()
        {
            serviceProvider = serviceCollection.BuildServiceProvider();
        }

        public T ResolveFirst<T>()
        {
            return serviceProvider.GetService<T>();
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return serviceProvider.GetServices<T>();
        }
    }
}
