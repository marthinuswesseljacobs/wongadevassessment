﻿using System.Collections.Generic;

namespace Component.Ioc
{
    public interface IIocComponent
    {
        void Register<TService, TImplementation>()
            where TService : class
            where TImplementation : class, TService;
        void Register<TService>(TService implementation)
            where TService : class;
        void Build();
        T ResolveFirst<T>();
        IEnumerable<T> ResolveAll<T>();
    }
}
